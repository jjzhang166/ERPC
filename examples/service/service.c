#include <stdio.h>
#include <sys/time.h>

#include "erpc.h"

cJSON *hello_world_service(cJSON *params)
{
    printf("Hello world!\n");
    return NULL;
}

int main(void)
{
    erpc_framework_init("service");

    erpc_service_register("hello", "helloworld", hello_world_service);

    return erpc_framework_loop(ERPC_LOOP_DEFAULT);
}

